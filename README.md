# Conan docker tools for Soleil

Based on https://github.com/conan-io/conan-docker-tools to build Docker images for Soleil environments

All images include:
- Conan 2.12.1 with Python 3.6.13
- Cmake 3.31.5
- Git 2.39.2

| Image               | OS             | Arch    | GCC                                          |
| ------------------- | -------------- | ------- | ---------------------------------------------|
| conan-el6-gcc44     | CentOS 6       | x86_64  | gcc (GCC) 4.4.7 20120313 (Red Hat 4.4.7-23)  |
| conan-el6-gcc44-x86 | CentOS 6       | i686    | gcc (GCC) 4.4.7 20120313 (Red Hat 4.4.7-23)  |
| conan-el6-gcc91     | CentOS 6       | x86_64  | gcc (GCC) 9.1.1 20190605 (Red Hat 9.1.1-2)   |
| conan-el6-gcc91-x86 | CentOS 6       | i686    | gcc (GCC) 9.1.1 20190605 (Red Hat 9.1.1-2)   |
| conan-el7-gcc48     | CentOS 7       | x86_64  | gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-44)  |
| conan-el7-gcc48-x86 | CentOS 7       | i686    | gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-44)  |
| conan-el7-gcc93     | CentOS 7       | x86_64  | gcc (GCC) 9.3.1 20200408 (Red Hat 9.3.1-2)   |
| conan-el7-gcc93-x86 | CentOS 7       | i686    | gcc (GCC) 9.3.1 20200408 (Red Hat 9.3.1-2)   |
| conan-el7-gcc112    | CentOS 7       | x86_64  | gcc (GCC) 11.2.1 20220127 (Red Hat 11.2.1-9) |
| conan-el7-gcc114    | CentOS 7       | x86_64  | gcc (conda-forge gcc 11.4.0-13) 11.4.0       |
| conan-el7-gcc115    | CentOS 7       | x86_64  | gcc (GCC) 11.5.0                             |
| conan-el9-gcc115    | Rocky Linux 9  | x86_64  | gcc (GCC) 11.5.0 20240719 (Red Hat 11.5.0-5) |

## Build 

### To build one image

```
docker-compose -f conan-el6-gcc44/docker-compose.yml build
```

### To build an image manually without docker-compose

```
docker build --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy -t conan-el6-gcc44 -f conan-el6-gcc44/Dockerfile .
```

### To build image without docker-compose with specific version of Conan

```
$ docker build --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy --build-arg CONAN_VERSION=2.12.1 -t conan-el6-gcc44 -f conan-el6-gcc44/Dockerfile .
```

## Launch

### To launch one with docker-compose
```
$ docker-compose -f conan-el6-gcc44/docker-compose.yml run --rm conan-el6-gcc44
```

### To launch one x86 with docker-compose
```
$ docker-compose -f conan-el6-gcc44-x86/docker-compose.yml run --rm conan-el6-gcc44-x86
```

### To launch one container without docker-compose

```
$ docker run --rm -ti -e http_proxy -e https_proxy -e no_proxy conan-el6-gcc44
```

### To launch one container x86 without docker-compose

```
$ docker run --rm -ti -e http_proxy -e https_proxy -e no_proxy --platform=linux/386 conan-el6-gcc44-x86
```

### To launch containers in detached mode with docker-compose

To launch all containers
```
$ docker-compose up -d
```

To launch one containers
```
$ docker-compose up -d conan-el6-gcc44
```

To connect into a container
```
$ docker exec -it conan-el6-gcc44 bash
```

To connect into a container x86
```
$ docker exec -it conan-el6-gcc44-x86 linux32 bash
```

## Known issues

### BuildKit 
When you build an image that depends of other, you can be unable to get dependency if buildKit is enable
```
 => ERROR [internal] load metadata for docker.io/library/conan-el6-gcc44-x86:latest                                                                                                                          1.5s
------
 > [internal] load metadata for docker.io/library/conan-el6-gcc44-x86:latest:
------
failed to solve: rpc error: code = Unknown desc = failed to solve with frontend dockerfile.v0: failed to create LLB definition: pull access denied, repository does not exist or may require authorization: server message: insufficient_scope: authorization failed
```

To fix it
```
export DOCKER_BUILDKIT=0
```

### CentOS 6 on WSL2
There is an issue when old Linux distribution as CentOS 6 with Docker on Windows with WSL2

The exit with return code 139
https://github.com/microsoft/WSL/issues/4694#issuecomment-556095344

Create a file %USERPROFILE%/.wslconfig with Unix end lines (LF)
```
[wsl2]
kernelCommandLine = vsyscall=emulate
```

For testing

```
$ docker run -ti centos:6 bash
```

