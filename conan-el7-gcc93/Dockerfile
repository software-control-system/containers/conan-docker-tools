FROM gitlab-registry.synchrotron-soleil.fr/software-control-system/containers/dev-docker-tools/dev-el7-gcc93

LABEL maintainer="Patrick Madela <patrick.madela@synchrotron-soleil.fr"

ARG PYTHON_VERSION=3.6.13 \
    CONAN_VERSION=2.12.1 \
    CMAKE_VERSION=3.31.5 \
    GIT_VERSION=2.39.2 \
    DEVTOOLS_ROOT=/usr/Local/devtools \
    OS=linux \
    ARCH=x86_64

ENV PYTHON_ROOT=${DEVTOOLS_ROOT}/python-${PYTHON_VERSION}-${OS}-${ARCH} \
    CONAN_ROOT=${DEVTOOLS_ROOT}/conan-${CONAN_VERSION}-${OS}-${ARCH} \
    CMAKE_ROOT=${DEVTOOLS_ROOT}/cmake-${CMAKE_VERSION}-${OS}-${ARCH} \
    GIT_ROOT=${DEVTOOLS_ROOT}/git-${GIT_VERSION}-${OS}-${ARCH} \
    PATH=${DEVTOOLS_ROOT}/bin:${DEVTOOLS_ROOT}/python-${PYTHON_VERSION}-${OS}-${ARCH}/bin:${DEVTOOLS_ROOT}/conan-${CONAN_VERSION}-${OS}-${ARCH}:${DEVTOOLS_ROOT}/cmake-${CMAKE_VERSION}-${OS}-${ARCH}/bin:${DEVTOOLS_ROOT}/git-${GIT_VERSION}-${OS}-${ARCH}/bin:${PATH} \
    LD_LIBRARY_PATH=${DEVTOOLS_ROOT}/python-${PYTHON_VERSION}-${OS}-${ARCH}/lib \
    CXX=/opt/rh/devtoolset-9/root/usr/bin/g++ \
    CC=/opt/rh/devtoolset-9/root/usr/bin/gcc

RUN groupadd g1001 -g 1001 \
    && groupadd g1000 -g 1000 \
    && groupadd g2000 -g 2000 \
    # Creation of conan user
    && useradd -ms /bin/bash conan -u 1000 -g 1001 -G 1000,2000,999 \
    # Add sudo for conan user
    && printf "conan:conan" | chpasswd \
    && printf "conan ALL= NOPASSWD: ALL\\n" >> /etc/sudoers \
    # Create devtools folder
    && mkdir -p ${DEVTOOLS_ROOT} \
    # Install python 3
    && curl -s -L -o ${PYTHON_ROOT}.zip https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/python3/-/jobs/artifacts/3.6.13/download?job=linux-x86_64 \
    && unzip -q ${PYTHON_ROOT}.zip -d ${DEVTOOLS_ROOT} \
    && rm ${PYTHON_ROOT}.zip \
    # Install git
    && curl -s -L -o ${GIT_ROOT}.zip https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/git/-/jobs/artifacts/2.39.2/download?job=linux-x86_64 \
    && unzip -q ${GIT_ROOT}.zip -d ${DEVTOOLS_ROOT} \
    && rm ${GIT_ROOT}.zip \
    # Install cmake
    && CMAKE_VERSION_MAJOR_MINOR=$(echo ${CMAKE_VERSION} | sed 's/^\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)$/\1\.\2/') \ 
    && curl -s https://cmake.org/files/v${CMAKE_VERSION_MAJOR_MINOR}/cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz | tar -xzf - -C ${DEVTOOLS_ROOT} \
        --exclude=bin/cmake-gui \
        --exclude=doc/cmake \
        --exclude=share/cmake-${CMAKE_VERSION_MAJOR_MINOR}/Help \
        --exclude=share/vim \
    # Install conan
    && curl -s -L -o ${CONAN_ROOT}.zip https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/conan/-/jobs/artifacts/2.12.1/download?job=linux-x86_64 \
    && unzip -q ${CONAN_ROOT}.zip -d ${DEVTOOLS_ROOT} \
    && rm ${CONAN_ROOT}.zip \
    # Install other tools
    && mkdir ${DEVTOOLS_ROOT}/bin \
    && curl -s -L -o ${DEVTOOLS_ROOT}/bin/jq https://github.com/jqlang/jq/releases/download/jq-1.7.1/jq-linux-amd64 \
    && curl -s -L -o ${DEVTOOLS_ROOT}/bin/yq https://github.com/mikefarah/yq/releases/download/v4.34.2/yq_linux_amd64 \
    && chmod +x ${DEVTOOLS_ROOT}/bin/*

COPY environment /etc/environment 
USER conan
WORKDIR /home/conan

ENTRYPOINT ["bash", "-c" ,"source scl_source enable devtoolset-9 && conan config install --verify-ssl false --type git https://gitlab.synchrotron-soleil.fr/software-control-system/factory/conan/conan2-config.git &&  \"$@\"", "-s"]

CMD ["bash"]
